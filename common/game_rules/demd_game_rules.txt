﻿debug_log = {
	default = debug_no
	debug_yes = { }
	debug_no = { }
}

archaeology = {
	default = archaeology_off
	archaeology_on = { }
	archaeology_off = { }
}

claim_fabrication = {
	default = claim_fabrication_sow
	claim_fabrication_sow = { }
	claim_fabrication_vanilla = { }
}
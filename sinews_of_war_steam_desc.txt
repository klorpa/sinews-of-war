[url=https://discord.gg/8mW5WtWp7m][img]https://i.imgur.com/vcjkAey.png[/img][/url]


[h1] Sinews of War [/h1] 
Sinews of War is a vanilla gameplay overhaul centered around a completely redone population-based economy. Our goal is to make CK3 a more realistic simulation of the medieval world with the minimum possible compromise to performance.

[h2]Updated for 1.5.1 Fleur-de-Lis / Royal Court[/h2]

[h2][url=https://discord.gg/8mW5WtWp7m]Sinews of War Discord[/url] [/h2]
[h2][url=https://discord.gg/hjpxPQvFPQ]CMH Discord[/url] [/h2]
[h2][url=https://gitlab.com/Vertimnus/sinews-of-war]Sinews of War GitLab[/url] [/h2]

[h2]Features[/h2] 
[list]
[*] Intricate Malthusian population system with births, deaths and migration both between cities and between cities and hinterland.
[*] All-new agricultural-focused economy featuring a detailed soil fertility model that takes into account terrain, water sources, weather, and more.
[*] Manage your realm with six new county-level edicts.
[*] Buildings, traditions, and innovations redone with new effects for the redone economy.
[*] Population based factors for culture and faith spread.
[*] Deadly plagues that spread county to county. Characters now contract plagues that are present in their location rather than randomly.
[*] Natural disasters: fires, floods, and avalanches can afflict your counties, potentially causing massive damage
[*] Choose between occupying, sacking, or razing when capturing a county, with serious consequences for the local population and infrastructure, and for cultural acceptance between your culture and that of the victims.
[*] Fervor Equilibrium: fervor will trend towards a value determined by your head of faith traits, holy sites occupied, and more.
[*] Vigor System: vigor is the cultural counterpart to fervor. High vigor has many scaling positive effects, and low vigor the opposite. The lower the vigor, the more likely cultural divergences become.
[*] Better personality trait selection: takes into account culture traditions, faith sins and virtues, guardian traits, parental traits, court type, and more.
[*] Cultures with a shared heritage group/family have increased baseline cultural acceptance
[*] Cultures with a shared language group/family have increased baseline cultural acceptance
[*] Languages in the same group/family as your native language are easier to learn
[*] AI prefers to adopt court languages more closely related to their native tongue
[/list]

[url=https://steamcommunity.com/sharedfiles/filedetails/?id=2775683808][img]https://i.imgur.com/vRqzGXG.png[/img][/url]

[h2]Compatibility[/h2] 
[list]
[*] [url=https://steamcommunity.com/sharedfiles/filedetails/?id=2775683808]The CMH Playlist is a ready-made collection of mods guaranteed to work with SoW[/url]
[*] [url=https://steamcommunity.com/workshop/filedetails/discussion/2566883856/3181237564264359127/]Known Compatible and Incompatible Mods[/url]  
[*] Visual mods should be mostly compatible with the exception of some GUI mods
[*] Gameplay mods may or may not be compatible. Safest would be to assume they're not until known otherwise.
[*] Map mods and total conversions always need a patch
[*] [url=https://gitlab.com/Vertimnus/sinews-of-war/-/wikis/Adding-Sinews-of-War-to-your-Total-Conversion] How to make a patch for Sinews of War - if you need help, PM us on discord![/url]
[/list]

[h2]Developers[/h2]  
[list]
[*] [url=https://steamcommunity.com/profiles/76561198077584919]Vertimnus[/url] 
[*] [url=https://steamcommunity.com/profiles/76561198106794332]Necro[/url] 
[*] dangitsdaaaaang
[*] Crimson
[*] Wesh
[*] [url=https://steamcommunity.com/profiles/76561198054370821]SEANSOKEN[/url]  
[/list]

[h2]Contributors[/h2] 
[list]
[*] zijistark: sage advice
[*] Castox: gui assistance
[*] Atreides: pixel art
[*] Crunbum: beta testing, MaA
[*] theodosius: writing
[*] cryden: writing
[*] tiber: writing
[/list]
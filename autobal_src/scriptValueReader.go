package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"
)

func readSVFile(file string) map[string]float64 {
	// open file from cmd line args

	thisFile, err := os.Open(file)
	if err != nil {
		fmt.Println("Failed to open file: " + file)
		log.Fatal(err)
	}
	// Initialize scanner
	scanner := bufio.NewScanner(thisFile)
	// open file from cmd line args

	name2val := make(map[string]float64)

	for scanner.Scan() {
		// get next line
		line := scanner.Text()
		fields := strings.Fields(line)
		if len(fields) > 2 {
			value, err := strconv.ParseFloat(fields[2], 64)
			if err == nil {
				name2val[fields[0]] = value
			}
		}
	}

	return name2val
}
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import sys

def data_verifier (start_year,end_year,counties,duchies,kingdoms,empires):
    df_pop = pd.read_csv('pop_data.csv',delimiter = ';')
    data_years = df_pop['Year'].unique()
    data_county = df_pop['County'].unique()
    data_duchy = df_pop['Duchy'].unique()
    data_kingdom = df_pop['Kingdom'].unique()
    data_empire = df_pop['Empire'].unique()
    for county in counties:
        if county not in data_county:
            print(f'{county} not in database')
    for duchy in duchies:
        if duchy not in data_duchy:
            print(f'{duchy} not in database')
    for kingdom in kingdoms:
        if kingdom not in data_kingdom:
            print(f'{kingdom} not in database')
    for empire in empires:
        if empire not in data_empire:
            print(f'{empire} not in database')
    #if not (str(start_year) in data_years):
    #    print(f'{start_year} out of range')
    #if not (str(end_year) in data_years):
    #    print(f'{end_year} out of range')

# Need to verify data_value
def query_tool (query_year,counties,duchies,kingdoms,empires,data_value,title):
    data_total = []
    data_mean = []
    df_pop = pd.read_csv('pop_data.csv',delimiter = ';')
    data_years = df_pop['Year'].unique()
    data_county = df_pop['County'].unique()
    data_duchy = df_pop['Duchy'].unique()
    data_kingdom = df_pop['Kingdom'].unique()
    data_empire = df_pop['Empire'].unique()

    if len(counties) == 0:
        counties = data_county
    if len(duchies) == 0:
        duchies = data_duchy
    if len(kingdoms) == 0:
        kingdoms = data_kingdom
    if len(empires) == 0:
        empires = data_empire
    for year in query_year:
        print(f'{data_value}; {year}')
        value_list = df_pop[(df_pop['Year'] == year)  & (df_pop['County'].isin(counties)) & (df_pop['Duchy'].isin(duchies)) & (df_pop['Kingdom'].isin(kingdoms)) & (df_pop['Empire'].isin(empires))][data_value]
        total = round(sum(value_list),2)
        mean = round(np.mean(value_list),2)
        data_total.append(total)
        data_mean.append(mean)
    return data_total,data_mean

def disease_death_calculator(query_year,counties,duchies,kingdoms,empires,title):
    data_total = []
    df_pop = pd.read_csv('pop_data.csv',delimiter = ';')
    data_years = df_pop['Year'].unique()
    data_county = df_pop['County'].unique()
    data_duchy = df_pop['Duchy'].unique()
    data_kingdom = df_pop['Kingdom'].unique()
    data_empire = df_pop['Empire'].unique()

    if len(counties) == 0:
        counties = data_county
    if len(duchies) == 0:
        duchies = data_duchy
    if len(kingdoms) == 0:
        kingdoms = data_kingdom
    if len(empires) == 0:
        empires = data_empire
    for year in query_year:
        print(f'disease_death_rate; {year}')
        disease_rate = df_pop[(df_pop['Year'] == year)  & (df_pop['County'].isin(counties)) & (df_pop['Duchy'].isin(duchies)) & (df_pop['Kingdom'].isin(kingdoms)) & (df_pop['Empire'].isin(empires))]['diesase_death_rate']
        disease_rate = disease_rate / 100
        pop = df_pop[(df_pop['Year'] == year)  & (df_pop['County'].isin(counties)) & (df_pop['Duchy'].isin(duchies)) & (df_pop['Kingdom'].isin(kingdoms)) & (df_pop['Empire'].isin(empires))]['pop']
        disease_death = np.multiply(disease_rate,pop)
        total_death = round(sum(disease_death),2)
        total_pop = sum(pop)
        disease_percent_of_pop = (total_death/total_pop) * 100
        data_total.append(disease_percent_of_pop)
    return data_total

def county_counter (query_year,counties,duchies,kingdoms,empires,data_value,title):
    data_percent = []
    df_pop = pd.read_csv('pop_data.csv',delimiter = ';')
    data_years = df_pop['Year'].unique()
    data_county = df_pop['County'].unique()
    data_duchy = df_pop['Duchy'].unique()
    data_kingdom = df_pop['Kingdom'].unique()
    data_empire = df_pop['Empire'].unique()
    total_county = len(data_county)
    if len(counties) == 0:
        counties = data_county
    if len(duchies) == 0:
        duchies = data_duchy
    if len(kingdoms) == 0:
        kingdoms = data_kingdom
    if len(empires) == 0:
        empires = data_empire
    for year in query_year:
        pop = df_pop[(df_pop['Year'] == year)  & (df_pop['County'].isin(counties)) & (df_pop['Duchy'].isin(duchies)) & (df_pop['Kingdom'].isin(kingdoms)) & (df_pop['Empire'].isin(empires)) & (df_pop[data_value] < 100) & (df_pop[data_value] > 0) ][data_value]
        count = len(pop)
        print(f'{year};{data_value}')
        percent = count / total_county
        data_percent.append(percent)
    return data_percent

def plot_pop (years,rural,urban,total,title):
    plt.title(f'Population for {title}')
    plt.xlabel('Year')
    plt.ylabel('Population')
    plt.plot(years,total,label = 'total')
    plt.plot(years,urban, label = 'urban')
    plt.plot(years,rural, label = 'rural')
    plt.legend()
    plt.savefig('pop_plot.png')
    plt.close()

def plot_districts (years,food,good,total,title):
    plt.title(f'districts for {title}')
    plt.xlabel('Year')
    plt.ylabel('Districts')
    plt.plot(years,total,label = 'total')
    plt.plot(years,food, label = 'food')
    plt.plot(years,good, label = 'good')
    plt.legend()
    plt.savefig('pop_district.png')
    plt.close()

def plot_building (years,feature_upkeep,boring_upkeep,gross_tax,title):
    plt.title(f'Building econ for {title}')
    plt.xlabel('Year')
    plt.ylabel('Gold')
    plt.plot(years,feature_upkeep,label = 'Total Building Upkeep')
    plt.plot(years,boring_upkeep, label = 'Sewers and Barracks')
    plt.plot(years,gross_tax, label = 'Gross Tax')
    plt.legend()
    plt.savefig('pop_buildings.png')
    plt.close()

def plot_maa_econ (years,maa_unraised_cost,maa_raised_cost,net_income,title):
    plt.title(f'MAA econ for {title}')
    plt.xlabel('Year')
    plt.ylabel('Gold')
    plt.plot(years,net_income,label = 'Net Income')
    plt.plot(years,maa_raised_cost, label = 'Raised Cost')
    plt.plot(years,maa_unraised_cost, label = 'Unraised Cost')
    plt.legend()
    plt.savefig('pop_maa.png')
    plt.close()

def plot_disease_death (years,disease_percent_of_pop,title):
    plt.title(f'Disease percent of pop for {title}')
    plt.xlabel('Year')
    plt.ylabel('Percentage')
    plt.plot(years,disease_percent_of_pop,label = 'Disease death percent')
    #plt.legend()
    plt.savefig('pop_disease.png')
    plt.close()

def plot_county_count(years,faith,culture,control):
    plt.title(f'Percentage of total county -  {title}')
    plt.xlabel('Year')
    plt.ylabel('Percentage')
    plt.plot(years,faith,label = 'Faith converting')
    plt.plot(years,culture,label = 'Culture converting')
    plt.plot(years,control,label = '0 < Control Limit < 100 ')
    plt.legend()
    plt.savefig('pop_count.png')
    plt.close()

if __name__ == '__main__':
    start_year = 1067
    end_year = 1444
    counties = []
    duchies = []
    kingdoms = []
    empires = []
    title = ''
    for argument in sys.argv[1:]:
        argument.strip()
        if start_year == 1067 and argument.isnumeric() :
            start_year = argument
        elif argument.isnumeric():
            end_year = argument
        elif argument.startswith('c_'):
            counties.append(argument)
            title = title + ' ' + argument
        elif argument.startswith('d_'):
            duchies.append(argument)
            title = title + ' ' + argument
        elif argument.startswith('k_'):
            kingdoms.append(argument)
            title = title + ' ' + argument
        elif argument.startswith('e_'):
            empires.append(argument)
            title = title + ' ' + argument
        else:
            print (f"Invalid Argument: {argument}")
    if title == '':
        title = 'World'
    data_verifier(start_year,end_year,counties,duchies,kingdoms,empires)
    query_year = np.arange(start = int(start_year), stop = int(end_year) + 1, step = 1)

    pop = query_tool(query_year,counties,duchies,kingdoms,empires,'pop',title)[0]
    urban_pop = query_tool(query_year,counties,duchies,kingdoms,empires,'urban_pop',title)[0]
    rural_pop = np.subtract(pop,urban_pop)
    plot_pop(query_year,rural_pop,urban_pop,pop,title)

    food_districts = query_tool(query_year,counties,duchies,kingdoms,empires,'food_districts',title)[0]
    good_districts = query_tool(query_year,counties,duchies,kingdoms,empires,'good_districts',title)[0]
    districts = np.add(food_districts,good_districts)
    plot_districts(query_year,food_districts,good_districts,districts,title)

    gross_tax = query_tool(query_year,counties,duchies,kingdoms,empires,'gross_tax',title)[1]
    feature_upkeep = query_tool(query_year,counties,duchies,kingdoms,empires,'feature_upkeep',title)[1]
    barracks_upkeep = query_tool(query_year,counties,duchies,kingdoms,empires,'barracks_upkeep',title)[1]
    sewer_upkeep = query_tool(query_year,counties,duchies,kingdoms,empires,'sewer_upkeep',title)[1]
    boring_upkeep = np.add(barracks_upkeep,sewer_upkeep)
    plot_building(query_year,feature_upkeep,boring_upkeep,gross_tax,title)

    net_income = np.subtract(gross_tax,feature_upkeep)
    maa_manpower = query_tool(query_year,counties,duchies,kingdoms,empires,'county_maa',title)[1]
    maa = np.true_divide(maa_manpower,60)
    maa_unraised_cost = np.dot(maa,0.3)
    maa_raised_cost = np.dot(maa,0.5)
    plot_maa_econ(query_year,maa_unraised_cost,maa_raised_cost,net_income,title)

    disease_death_rate = disease_death_calculator(query_year,counties,duchies,kingdoms,empires,title)
    plot_disease_death(query_year,disease_death_rate,title)

    faith_count = county_counter(query_year,counties,duchies,kingdoms,empires,'faith_conversion_progress',title)
    culture_count = county_counter(query_year,counties,duchies,kingdoms,empires,'culture_conversion_progress',title)
    control_count = county_counter(query_year,counties,duchies,kingdoms,empires,'control_limit',title)
    plot_county_count(query_year,faith_count,culture_count,control_count)
